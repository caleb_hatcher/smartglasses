package com.vuzix.sample.m300_speech_recognition;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class Quantity extends Activity implements VoiceActionListener {
    public final String CUSTOM_SDK_INTENT = "com.vuzix.sample.m300_voicecontrolwithsdk.CustomIntent";
    VoiceCmdReceiver mVoiceCmdReceiver;
    EditText qtyField;
    Button ctnButton;
    TextView locText;
    TextView partText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quantity);
        qtyField = (EditText) findViewById(R.id.qtyField);
        ctnButton = (Button) findViewById(R.id.continueButton);
        locText = (TextView) findViewById(R.id.locationText);
        partText = (TextView) findViewById(R.id.partText);
        // Create the voice command receiver class
        mVoiceCmdReceiver = new VoiceCmdReceiver(this, this);
        if (!mVoiceCmdReceiver.getActive()) {
            mVoiceCmdReceiver.TriggerRecognizerToListen(true);
        }
        partText.setText("Part Number: " + Variables.partNumber );
        locText.setText("Location: " + Variables.location);

    }


    /**
     * Unregister from the speech SDK
     */
    @Override
    protected void onDestroy() {
        mVoiceCmdReceiver.unregister();
        super.onDestroy();
    }

    @Override
    public void OnEnterClick() {
        qtyField.requestFocus();
        Log.e("Working", "QUANTITY");
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    @Override
    public void SelectTextBox() {
        return;
    }

    @Override
    public void NextPage() {
        return;
    }

    @Override
    public void OnClearClick() {
        return;
    }

    @Override
    public void OnPopupClick() {
        return;
    }

    @Override
    public void OnRestoreClick() {
        return;
    }

    @Override
    public void OnDone() {
        Log.d("OnDone", "Enter");

        //ctnButton.requestFocusFromTouch();
        OpenDetails(null);
        return;
    }

    public void OpenDetails(View view) {
        Log.d("OpenDetails", "Enter");
        mVoiceCmdReceiver.unregister();
        if (ctnButton.getText().length() < 1){
            Toast myToast = Toast.makeText(this, "Need to enter a quantity.", Toast.LENGTH_LONG);
            myToast.show();
        } else {
            Intent newActivity = new Intent(this, ProductDetails.class);
            startActivity(newActivity);
            finish();
        }

    }




}
