package com.vuzix.sample.m300_speech_recognition;

import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.widget.EditText;
import com.vuzix.sdk.barcode.ScanResult;
import com.vuzix.sdk.barcode.ScannerIntent;
import android.content.Intent;
import android.widget.Toast;
import android.content.ActivityNotFoundException;
import android.widget.Button;
import android.view.View;




public class SecondActivity extends Activity {
    private static final int REQUEST_CODE_SCAN = 90002;  // Must be unique within this Activity
    private EditText scanField;
    private Button mButtonScan;
    private Button mButtonNext;
    VoiceCmdReceiver VoiceReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        scanField = (EditText) findViewById(R.id.edit_textBox);
        Log.e("field", "Text" + scanField);
        mButtonNext = (Button) findViewById(R.id.next_button);


        mButtonScan = (Button) findViewById(R.id.scan_button);
        mButtonScan.requestFocusFromTouch();
        mButtonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {
                OnScanClick();
            }
        });

        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {
                OnNextClick();
            }
        });
    }

    /**
     * Handler for the button press. Activates the scan.
     */
    private void OnScanClick() {
        Intent scannerIntent = new Intent(ScannerIntent.ACTION);
        try {
            // The Vuzix M300 has a built-in Barcode Scanner app that is registered for this intent.
            startActivityForResult(scannerIntent, REQUEST_CODE_SCAN);
        } catch (ActivityNotFoundException activityNotFound) {
            Toast.makeText(this, R.string.only_on_m300, Toast.LENGTH_LONG).show();
        }
    }

    private void OnNextClick() {
        Intent startNewActivity = new Intent(this, LocationScreen.class);
        startActivity(startNewActivity);
    }


    /**
     * The M300 Barcode Scanner App will scan a barcode and return
     *
     * @param requestCode int identifier you provided in startActivityForResult
     * @param resultCode int result of the scan operation
     * @param data Intent containing a ScanResult whenver the resultCode indicates RESULT_OK
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SCAN:
                if (resultCode == Activity.RESULT_OK) {
                    ScanResult scanResult = data.getParcelableExtra(ScannerIntent.RESULT_EXTRA_SCAN_RESULT);
                    Log.e("Scan", "Text:: " + scanResult.getText());
                    Variables.partNumber = scanResult.getText();
                    Variables.location = "AF34000A00";
                    scanField.setText( scanResult.getText() );
                    mButtonNext.setVisibility(View.VISIBLE);
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
