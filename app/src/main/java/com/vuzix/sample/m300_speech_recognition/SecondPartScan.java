package com.vuzix.sample.m300_speech_recognition;

import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.widget.EditText;
import com.vuzix.sdk.barcode.ScanResult;
import com.vuzix.sdk.barcode.ScannerIntent;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;
import android.content.ActivityNotFoundException;
import android.view.View;




public class SecondPartScan extends Activity {
    private static final int REQUEST_CODE_SCAN = 90005;  // Must be unique within this Activity
    private EditText scanField;
    private TextView partTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_part_scan);

        scanField = (EditText) findViewById(R.id.scan_text_field);
        partTitle = (TextView) findViewById(R.id.scan_title);
        Log.e("SECOND", "PartScan" + scanField);

    }

    /**
     * Handler for the button press. Activates the scan.
     */
    public void OnScanClick(View view) {
        Intent scannerIntent = new Intent(ScannerIntent.ACTION);
        try {
            // The Vuzix M300 has a built-in Barcode Scanner app that is registered for this intent.
            startActivityForResult(scannerIntent, REQUEST_CODE_SCAN);
        } catch (ActivityNotFoundException activityNotFound) {
            Toast.makeText(this, R.string.only_on_m300, Toast.LENGTH_LONG).show();
        }
    }

    public void OpenQuantityScreen() {
        Intent newActivity = new Intent(this, Quantity.class);
        startActivity(newActivity);
    }


    /**
     * The M300 Barcode Scanner App will scan a barcode and return
     *
     * @param requestCode int identifier you provided in startActivityForResult
     * @param resultCode int result of the scan operation
     * @param data Intent containing a ScanResult whenver the resultCode indicates RESULT_OK
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SCAN:
                if (resultCode == Activity.RESULT_OK) {
                    ScanResult scanResult = data.getParcelableExtra(ScannerIntent.RESULT_EXTRA_SCAN_RESULT);
                    Log.e("Scan", "Text:: " + scanResult.getText());

                    scanField.setText( scanResult.getText() );
                    Variables.iid = "000124 6785932";
                    Log.e("Variables", Variables.partNumber);
                    Log.e("scanresult", scanResult.getText());
                   // if (scanResult.getText().toString() != Variables.partNumber) {
                     //   partTitle.setText("Error Part Number doesn't match scan again.");
                       // partTitle.setTextColor(Color.RED);
                    //} else {
                        OpenQuantityScreen();
                   // }
                    //mButtonNext.setVisibility(View.VISIBLE);
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

