package com.vuzix.sample.m300_speech_recognition;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.vuzix.sample.m300_speech_recognition.R;
import com.vuzix.sdk.barcode.ScanResult;
import com.vuzix.sdk.barcode.ScannerFragment;
import com.vuzix.sdk.barcode.ScanningRect;

import java.io.IOException;

public class ScannerContainer extends Activity {
    TextView locationText;
    TextView descriptionText;
    ProgressDialog loading;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loading = new ProgressDialog(this);
        setContentView(R.layout.activity_scanner_container);
        locationText = (TextView) findViewById(R.id.scanText);
        descriptionText = (TextView) findViewById(R.id.descText);
        locationText.setText(Variables.location);
        final ScannerFragment scannerFragment = new ScannerFragment();

        loading.setTitle("Loading");
        loading.setMessage("We are processing the location...");
        loading.setCancelable(false); // disable dismiss by tapping outside of the dialog

        //Intent scannerIntent = new Intent(ScannerIntent.ACTION);
        Bundle args = new Bundle();
        args.putParcelable(ScannerFragment.ARG_SCANNING_RECT, new ScanningRect(.6f, .75f));
        scannerFragment.setArguments(args);
        getFragmentManager().beginTransaction().add(R.id.fragment_container, scannerFragment).commit();
        scannerFragment.setListener(new ScannerFragment.Listener() {
            @Override
            public void onScanResult(Bitmap bitmap, ScanResult[] scanResults) {
                scannerFragment.setListener(null);
                beep();
                Variables.partNumber = scanResults[0].getText();
                loading.show();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                loading.dismiss();
                                locationText.setText(Variables.partNumber);
                                descriptionText.setText("Scan Part Number  ");
                                scannerFragment.setListener(new secondScannerListener());
                            }
                        },
                1000);

                //ScanResult scanResult = scanResults.getParcelableExtra(ScannerIntent.RESULT_EXTRA_SCAN_RESULT);
                    /*if (scanResult.getText() != Variables.location) {
                        popupToast("Code doesn't match location.");
                    } else {
                        Log.e("Scan", "Text:: " + scanResult.getText());
                        showDialog("You have scanned your location.  Now please scan your part number.");
                    }*/

                // Uncomment out the above if we want to check against given location.
                Log.e("Scan", "Text:: " + scanResults[0].getText());
            }

            @Override
            public void onError() {

            }
        });

    }

    class secondScannerListener implements ScannerFragment.Listener {
        @Override
        public void onScanResult(Bitmap bitmap, ScanResult[] results) {
            ScannerFragment scannerFragment = (ScannerFragment) getFragmentManager().findFragmentById(R.id.fragment_container);
            scannerFragment.setListener(null);
            loading.setMessage("We are processing your part number.");
            loading.show();
            beep();
            new  android.os.Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            loading.dismiss();
                            Intent startNewActivity = new Intent(ScannerContainer.this, Quantity.class);
                            startActivity(startNewActivity);
                        }
                    }
            , 750);


        }

        @Override
        public void onError() {

        }
    }

    public void showDialog(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void beep() {
        MediaPlayer player = new MediaPlayer();
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });
        try {
            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            player.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
            file.close();
            player.setVolume(.1f, .1f);
            player.prepare();
            player.start();
        } catch (IOException e) {
            player.release();
        }
    }
}
