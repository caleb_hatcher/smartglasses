/* 
Copyright (c) 2017, Vuzix Corporation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

*  Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
    
*  Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
    
*  Neither the name of Vuzix Corporation nor the names of
   its contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
    
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.vuzix.sample.m300_speech_recognition;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.vuzix.sdk.speechrecognitionservice.VuzixSpeechClient;

import static android.view.KeyEvent.KEYCODE_A;
import static android.view.KeyEvent.KEYCODE_AT;
import static android.view.KeyEvent.KEYCODE_B;
import static android.view.KeyEvent.KEYCODE_C;
import static android.view.KeyEvent.KEYCODE_CAPS_LOCK;
import static android.view.KeyEvent.KEYCODE_D;
import static android.view.KeyEvent.KEYCODE_DEL;
import static android.view.KeyEvent.KEYCODE_E;
import static android.view.KeyEvent.KEYCODE_ENTER;
import static android.view.KeyEvent.KEYCODE_F;
import static android.view.KeyEvent.KEYCODE_G;
import static android.view.KeyEvent.KEYCODE_H;
import static android.view.KeyEvent.KEYCODE_I;
import static android.view.KeyEvent.KEYCODE_J;
import static android.view.KeyEvent.KEYCODE_K;
import static android.view.KeyEvent.KEYCODE_L;
import static android.view.KeyEvent.KEYCODE_M;
import static android.view.KeyEvent.KEYCODE_N;
import static android.view.KeyEvent.KEYCODE_O;
import static android.view.KeyEvent.KEYCODE_P;
import static android.view.KeyEvent.KEYCODE_PERIOD;
import static android.view.KeyEvent.KEYCODE_Q;
import static android.view.KeyEvent.KEYCODE_R;
import static android.view.KeyEvent.KEYCODE_S;
import static android.view.KeyEvent.KEYCODE_SHIFT_LEFT;
import static android.view.KeyEvent.KEYCODE_SPACE;
import static android.view.KeyEvent.KEYCODE_T;
import static android.view.KeyEvent.KEYCODE_U;
import static android.view.KeyEvent.KEYCODE_V;
import static android.view.KeyEvent.KEYCODE_W;
import static android.view.KeyEvent.KEYCODE_X;
import static android.view.KeyEvent.KEYCODE_Y;
import static android.view.KeyEvent.KEYCODE_Z;
import static android.view.KeyEvent.KEYCODE_0;
import static android.view.KeyEvent.KEYCODE_1;
import static android.view.KeyEvent.KEYCODE_2;
import static android.view.KeyEvent.KEYCODE_3;
import static android.view.KeyEvent.KEYCODE_4;
import static android.view.KeyEvent.KEYCODE_5;
import static android.view.KeyEvent.KEYCODE_6;
import static android.view.KeyEvent.KEYCODE_7;
import static android.view.KeyEvent.KEYCODE_8;
import static android.view.KeyEvent.KEYCODE_9;

/**
 * Class to encapsulate all voice commands
 */
public class VoiceCmdReceiver  extends BroadcastReceiver {
    // Voice command substitutions. These substitutions are returned when phrases are recognized.
    // This is done by registering a phrase with a substition. This eliminates localization issues
    // and is encouraged
    final String MATCH_POPUP = "popup";
    final String MATCH_CLEAR = "clear";
    final String MATCH_RESTORE = "restore";
    final String MATCH_EDIT_TEXT = "edit_text";
    final String MATCH_ENTER = "enter";
    final String MATCH_TYPE = "type";
    final String MATCH_NEXT = "next";
    final String LOG_TAG = "VoiceReciever";
    final String MATCH_DONE = "done";
    final String MATCH_FINISH = "finish";
    final String MATCH_OKAY = "okay";
    private VoiceActionListener listener;
    boolean isRecognizerActive = false;
    // Voice command custom intent names
    final String TOAST_EVENT = "other_toast";

    private Activity mMainActivity;

    /**
     * Constructor which takes care of all speech recognizer registration
     * @param iActivity MainActivity from which we are created
     */
    public VoiceCmdReceiver(Activity iActivity, VoiceActionListener listener)
    {
        mMainActivity = iActivity;
        this.listener = listener;
        mMainActivity.registerReceiver(this, new IntentFilter(VuzixSpeechClient.ACTION_VOICE_COMMAND));
        Log.d(LOG_TAG, "Connecting to M300 SDK");

        try {
            // Create a VuzixSpeechClient from the SDK
            VuzixSpeechClient sc = new VuzixSpeechClient(iActivity);
            // Delete specific phrases. This is useful if there are some that sound similar to yours, but
            // you want to keep the majority of them intact
            //sc.deletePhrase("torch on");
            //sc.deletePhrase("torch on");

            // Delete every phrase in the dictionary! (Available in SDK version 3)
            sc.deletePhrase("*");

            // Now add any new strings.  If you put a substitution in the second argument, you will be passed that string instead of the full string
            sc.insertKeycodePhrase("Alfa", KEYCODE_A );
            sc.insertKeycodePhrase("Bravo", KEYCODE_B);
            sc.insertKeycodePhrase("Charlie", KEYCODE_C);
            sc.insertKeycodePhrase("Delta", KEYCODE_D);
            sc.insertKeycodePhrase("Echo", KEYCODE_E);
            sc.insertKeycodePhrase("Foxtrot", KEYCODE_F);
            sc.insertKeycodePhrase("Golf", KEYCODE_G);
            sc.insertKeycodePhrase("Hotel", KEYCODE_H);
            sc.insertKeycodePhrase("India", KEYCODE_I);
            sc.insertKeycodePhrase("Juliett", KEYCODE_J);
            sc.insertKeycodePhrase("Kilo", KEYCODE_K);
            sc.insertKeycodePhrase("Lima", KEYCODE_L);
            sc.insertKeycodePhrase("Mike", KEYCODE_M);
            sc.insertKeycodePhrase("November", KEYCODE_N);
            sc.insertKeycodePhrase("Oscar", KEYCODE_O);
            sc.insertKeycodePhrase("Papa", KEYCODE_P);
            sc.insertKeycodePhrase("Quebec", KEYCODE_Q);
            sc.insertKeycodePhrase("Romeo", KEYCODE_R);
            sc.insertKeycodePhrase("Sierra", KEYCODE_S);
            sc.insertKeycodePhrase("Tango", KEYCODE_T);
            sc.insertKeycodePhrase("Uniform", KEYCODE_U);
            sc.insertKeycodePhrase("Victor", KEYCODE_V);
            sc.insertKeycodePhrase("Whiskey", KEYCODE_W);
            sc.insertKeycodePhrase("X-Ray", KEYCODE_X);
            sc.insertKeycodePhrase("Yankee", KEYCODE_Y);
            sc.insertKeycodePhrase("Zulu", KEYCODE_Z);
            sc.insertKeycodePhrase("Space", KEYCODE_SPACE);
            sc.insertKeycodePhrase("shift", KEYCODE_SHIFT_LEFT);
            sc.insertKeycodePhrase("caps lock", KEYCODE_CAPS_LOCK);
            sc.insertKeycodePhrase("at sign", KEYCODE_AT);
            sc.insertKeycodePhrase("period", KEYCODE_PERIOD);
            sc.insertKeycodePhrase("erase", KEYCODE_DEL);
            sc.insertKeycodePhrase("Zero", KEYCODE_0);
            sc.insertKeycodePhrase("One", KEYCODE_1);
            sc.insertKeycodePhrase("Two", KEYCODE_2);
            sc.insertKeycodePhrase("Three", KEYCODE_3);
            sc.insertKeycodePhrase("Four", KEYCODE_4);
            sc.insertKeycodePhrase("Five", KEYCODE_5);
            sc.insertKeycodePhrase("Six", KEYCODE_6);
            sc.insertKeycodePhrase("Seven", KEYCODE_7);
            sc.insertKeycodePhrase("Eight", KEYCODE_8);
            sc.insertKeycodePhrase("Nine", KEYCODE_9);

            // Insert a custom intent.  Note: these are sent with sendBroadcastAsUser() from the service
            // If you are sending an event to another activity, be sure to test it from the adb shell
            // using: am broadcast -a "<your intent string>"
            // This example sends it to ourself, and we are sure we are active and registered for it
            Intent customToastIntent = new Intent(Constants.CUSTOM_SDK_INTENT);
            sc.defineIntent(TOAST_EVENT, customToastIntent );
            sc.insertIntentPhrase("canned toast", TOAST_EVENT);

            // Insert phrases for our broadcast handler
            //
            // ** NOTE **
            // The "s:" is required in the SDK version 2, but is not required in the latest JAR distribution
            // or SDK version 3.  But it is harmless when not required. It indicates that the recognizer is making a
            // substitution.  When the multi-word string is matched (in any language) the associated MATCH string
            // will be sent to the BroadcastReceiver
            sc.insertPhrase(mMainActivity.getResources().getString(R.string.btn_text_pop_up),  MATCH_POPUP);
            sc.insertPhrase(mMainActivity.getResources().getString(R.string.btn_text_restore), MATCH_RESTORE);
            sc.insertPhrase(mMainActivity.getResources().getString(R.string.btn_text_clear),   MATCH_CLEAR);
            sc.insertPhrase("Edit Text", MATCH_EDIT_TEXT);
            sc.insertPhrase("Enter Text", MATCH_ENTER);
            sc.insertPhrase("Type Text", MATCH_TYPE);
            sc.insertPhrase("Next", MATCH_NEXT);
            sc.insertPhrase("Done", MATCH_DONE);
            sc.insertPhrase("Finish", MATCH_FINISH);
            sc.insertPhrase("Okay", MATCH_OKAY);


            // See what we've done
            Log.i("VOICEcmdR", sc.dump());

            // The recognizer may not yet be enabled in Settings. We can enable this directly
            VuzixSpeechClient.EnableRecognizer(mMainActivity, true);
        } catch(NoClassDefFoundError e) {
            // We get this exception if the SDK stubs against which we compiled cannot be resolved
            // at runtime. This occurs if the code is not being run on an M300 supporting the voice
            // SDK
            Toast.makeText(iActivity, R.string.only_on_m300, Toast.LENGTH_LONG).show();
            Log.e(LOG_TAG, iActivity.getResources().getString(R.string.only_on_m300) );
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
            iActivity.finish();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error setting custom vocabulary: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * All custom phrases registered with insertPhrase() are handled here.
     *
     * Custom intents may also be directed here, but this example does not demonstrate this.
     *
     * Keycodes are never handled via this interface
     *
     * @param context Context in which the phrase is handled
     * @param intent Intent associated with the recognized phrase
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.e(LOG_TAG, mMainActivity.getMethodName());
        // All phrases registered with insertPhrase() match ACTION_VOICE_COMMAND as do
        // recognizer status updates
        if (intent.getAction().equals(VuzixSpeechClient.ACTION_VOICE_COMMAND)) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                // We will determine what type of message this is based upon the extras provided
                if (extras.containsKey(VuzixSpeechClient.PHRASE_STRING_EXTRA)) {
                    // If we get a phrase string extra, this was a recognized spoken phrase.
                    // The extra will contain the text that was recognized, unless a substitution
                    // was provided.  All phrases in this example have substitutions as it is
                    // considered best practice
                    String phrase = intent.getStringExtra(VuzixSpeechClient.PHRASE_STRING_EXTRA);
                    //Log.e(LOG_TAG, mMainActivity.getMethodName() + " \"" + phrase + "\"");
                    // Determine the specific phrase that was recognized and act accordingly
                    if (phrase.equals(MATCH_POPUP)) {
//                        mMainActivity.OnPopupClick();
                        listener.OnPopupClick();
                    } else if (phrase.equals(MATCH_RESTORE)) {
                        listener.OnRestoreClick();
                    } else if (phrase.equals(MATCH_CLEAR)) {
                        listener.OnClearClick();
                    } else if (phrase.equals(MATCH_EDIT_TEXT)) {
                        listener.SelectTextBox();
                    } else if (phrase.equals(MATCH_ENTER)){
                        listener.OnEnterClick();
                    } else if (phrase.equals(MATCH_NEXT)) {
                        listener.NextPage();
                        unregister();
                    } else if (phrase.equals(MATCH_DONE) || phrase.equals(MATCH_FINISH) || phrase.equals(MATCH_OKAY)){
                        listener.OnDone();
                        unregister();
                    } else {
                        Log.e(LOG_TAG, "Phrase not handled " + phrase);
                    }
                } else if (extras.containsKey(VuzixSpeechClient.RECOGNIZER_ACTIVE_BOOL_EXTRA)) {
                    Log.e(LOG_TAG, "ELSE ");
                    // if we get a recognizer active bool extra, it means the recognizer was
                    // activated or stopped
                    isRecognizerActive = extras.getBoolean(VuzixSpeechClient.RECOGNIZER_ACTIVE_BOOL_EXTRA, false);
                    //mMainActivity.RecognizerChangeCallback(isRecognizerActive);
                }
            }
        }
    }

    /**
     * Called to unregister for voice commands. An important cleanup step.
     */
    public void unregister() {
        isRecognizerActive = false;
        //Log.d(mMainActivity.LOG_TAG, "Unregistered");
        try {
            mMainActivity.unregisterReceiver(this);
            //Log.i(mMainActivity.LOG_TAG, "Custom vocab removed");
            mMainActivity = null;
        }catch (Exception e) {
            //Log.e(mMainActivity.LOG_TAG, "Custom vocab died " + e.getMessage());
        }
    }


    public boolean getActive() {
       return isRecognizerActive;
    }

    /**
     * Handler called when "Listen" button is clicked. Activates the speech recognizer identically to
     * saying "Hello Vuzix"
     *
     * @param bOnOrOff boolean True to enable listening, false to cancel it
     */
    public void TriggerRecognizerToListen(boolean bOnOrOff) {
        try {
            VuzixSpeechClient.TriggerVoiceAudio(mMainActivity, bOnOrOff);
            isRecognizerActive = bOnOrOff;
        } catch (NoClassDefFoundError e) {
            // The voice SDK was added in version 2. The constructor will have failed if the
            // target device is not an M300 that is compatible with SDK version 2.  But the trigger
            // command with the bool was added in SDK version 4.  It is possible the M300 does not
            // yet have the TriggerVoiceAudio interface. If so, we get this exception.
            Toast.makeText(mMainActivity, R.string.upgrade_m300, Toast.LENGTH_LONG).show();
        }
    }

}
