package com.vuzix.sample.m300_speech_recognition;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import org.w3c.dom.Text;

public class LocationScreen extends Activity {
    private TextView locationLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_screen);

        locationLabel = (TextView) findViewById(R.id.LocationLabel);
        Log.e("Location", Variables.location);
        locationLabel.setText(Variables.location);
    }

    public void continueClick(View v) {
        Log.w("Location", "Continue Click");
        Intent nextPage = new Intent(this, ScanLocation.class);
        startActivity(nextPage);
    }
}
