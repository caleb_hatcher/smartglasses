package com.vuzix.sample.m300_speech_recognition;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public class ProductDetails extends Activity implements VoiceActionListener {
    private TextView locationText;

    VoiceCmdReceiver mVoiceCmdReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        locationText = (TextView) findViewById(R.id.locationText);
        mVoiceCmdReceiver = new VoiceCmdReceiver(this, this);;

        locationText.setText( Variables.location);

    }


    public void finishClick(View view) {
        this.finishAffinity();
    }


    public void nextCount(View view) {
        mVoiceCmdReceiver.unregister();
        finish();
        Intent newActivity = new Intent(this, MainActivity.class);
        startActivity(newActivity);
    }


    @Override
    public void SelectTextBox() {
        return;
    }

    @Override
    public void NextPage() {
        mVoiceCmdReceiver.unregister();
        nextCount(null);
    }

    @Override
    public void OnClearClick() {
        return;
    }

    @Override
    public void OnPopupClick() {
        return;
    }

    @Override
    public void OnRestoreClick() {
        return;
    }

    @Override
    public void OnDone() {
        finishClick(null);
    }

    @Override
    public void OnEnterClick() {
        return;
    }

}
