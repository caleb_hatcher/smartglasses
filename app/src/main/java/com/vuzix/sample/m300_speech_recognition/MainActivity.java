/*
Copyright (c) 2017, Vuzix Corporation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

*  Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
    
*  Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
    
*  Neither the name of Vuzix Corporation nor the names of
   its contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
    
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.vuzix.sample.m300_speech_recognition;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.vuzix.sdk.barcode.ScanResult;
import com.vuzix.sdk.barcode.ScannerIntent;
import com.vuzix.sdk.barcode.ScannerFragment;
import android.app.AlertDialog;
import android.content.DialogInterface;

import java.util.Random;

import com.vuzix.sdk.barcode.ScanningRect;
/**
 * Main activity for speech recognition sample
 */
public class MainActivity extends Activity implements VoiceActionListener {
    private static final int REQUEST_CODE_SCAN = 90000;
    private static final int PART_CODE_SCAN = 90001;
    private static final int PERMISSION_REQUEST_CAMERA = 40000;

    public final String LOG_TAG = "VoiceSample";
    public final String CUSTOM_SDK_INTENT = "com.vuzix.sample.m300_voicecontrolwithsdk.CustomIntent";
    VoiceCmdReceiver mVoiceCmdReceiver;
    private boolean mRecognizerActive = false;
    Button nextButton;
    TextView locationText;
    String[] locations;
    Random r;
    int randomNum;
    AlertDialog alertDialog;

    /**
     * when created we setup the layout and the speech recognition
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nextButton = (Button) findViewById(R.id.next_button);
        locationText = (TextView) findViewById(R.id.locationView);
        locations = new String[]{"ABC123WW3", "ABC123W10", "ABC562WW3", "ABC10123", "ABC123A04", "ABC123A03", "ABD402048", "ABC102A09", "ABC123MU3", "ABC123LF3", "ABC123QR3"};
        r = new Random();
        randomNum = r.nextInt(10);
        Variables.location = locations[randomNum];
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        locationText.setText(Variables.location);
        // It is a best practice to explicitly request focus to a button to make navigation with the
        // M300 buttons more consistent to the user
        //buttonListen.requestFocusFromTouch();

        requestPermissions(new String[]{ Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {
                NextPage();
            }
        });
        // Create the voice command receiver class
        mVoiceCmdReceiver = new VoiceCmdReceiver(this, this);
        mVoiceCmdReceiver.TriggerRecognizerToListen(true);
        // Now register another intent handler to demonstrate intents sent from the service
        myIntentReceiver = new MyIntentReceiver();
        registerReceiver(myIntentReceiver , new IntentFilter(CUSTOM_SDK_INTENT));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("main", "Location permission accepted");
            } else {
                Log.d("main", "Location permission denied");
            }
        }
    }


    /**
     * Unregister from the speech SDK
     */
    @Override
    protected void onDestroy() {
        mVoiceCmdReceiver.unregister();
        super.onDestroy();
    }


    /**
     * Utility to get the name of the current method for logging
     * @return String name of the current method
     */
    public String getMethodName() {
        return LOG_TAG + ":" + this.getClass().getSimpleName() + "." + new Throwable().getStackTrace()[1].getMethodName();
    }

    /**
     * Helper to show a toast
     * @param iStr String message to place in toast
     */
    private void popupToast(String iStr) {
        Toast myToast = Toast.makeText(MainActivity.this, iStr, Toast.LENGTH_LONG);
        myToast.show();
    }

    /**
     * Update the button from "Listen" to "Stop" based on our cached state
     */
    private void updateListenButtonText() {
        int newText = R.string.btn_text_listen;
        if ( mRecognizerActive ) {
            newText = R.string.btn_text_stop;
        }
    }

    /**
     * Handler called when "Listen" button is clicked. Activates the speech recognizer identically to
     * saying "Hello Vuzix".  Also handles "Stop" button clicks to terminate the recognizer identically
     * to a time-out
     */
    private void OnListenClick() {
        Log.e(LOG_TAG, getMethodName());
        // Trigger the speech recognizer to start/stop listening.  Listening has a time-out
        // specified in the M300 settings menu, so it may terminate without us requesting it.
        //
        // We want this to toggle to state opposite our current one.
        mRecognizerActive = !mRecognizerActive;
        // Manually calling this syncrhonizes our UI state to the recognizer state in case we're
        // requesting the current state, in which we won't be notified of a change.
        updateListenButtonText();
        // Request the new state
        mVoiceCmdReceiver.TriggerRecognizerToListen(mRecognizerActive);
    }

    /**
     * Sample handler that will be called from the "popup message" button, or a voice command
     */
    @Override
    public void OnPopupClick() {
        Log.e(LOG_TAG, getMethodName());
        //popupToast(textEntryField.getText().toString());
    }

    /**
     * Sample handler that will be called from the "clear" button, or a voice command
     */
    @Override
    public void OnClearClick() {
        Log.e(LOG_TAG, getMethodName());
        //textEntryField.setText("");
    }

    @Override
    public void OnEnterClick() {
        Log.e(LOG_TAG, getMethodName());
        //textEntryField.setText("");
    }
    @Override
    public void NextPage() {
        Log.d("Main", "NextPage");
        mVoiceCmdReceiver.unregister();
        Intent newActivity = new Intent(this, ScannerContainer.class);
        startActivity(newActivity);

    }

    /**
     * The M300 Barcode Scanner App will scan a barcode and return
     *
     * @param requestCode int identifier you provided in startActivityForResult
     * @param resultCode int result of the scan operation
     * @param data Intent containing a ScanResult whenver the resultCode indicates RESULT_OK
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SCAN:
                if (resultCode == Activity.RESULT_OK) {
                    ScanResult scanResult = data.getParcelableExtra(ScannerIntent.RESULT_EXTRA_SCAN_RESULT);
                    /*if (scanResult.getText() != Variables.location) {
                        popupToast("Code doesn't match location.");
                    } else {
                        Log.e("Scan", "Text:: " + scanResult.getText());
                        showDialog("You have scanned your location.  Now please scan your part number.");
                    }*/
                    // Uncomment out the above if we want to check against given location.
                    Log.e("Scan", "Text:: " + scanResult.getText());
                    showDialog("You have scanned the correct location.  Now please scan part number: 01468902742");
                }
                return;
            case PART_CODE_SCAN:
                if (resultCode == Activity.RESULT_OK) {
                    ScanResult scanResult = data.getParcelableExtra(ScannerIntent.RESULT_EXTRA_SCAN_RESULT);
                    Log.e("Scan@2", "Text:: " + scanResult.getText());
                    Variables.partNumber = scanResult.getText();
                    Intent startNewActivity = new Intent(this, Quantity.class);
                    startActivity(startNewActivity);
                    finish();

                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Sample handler that will be called from the "restore" button, or a voice command
     */
    @Override
    public void OnRestoreClick() {
        Log.e(LOG_TAG, getMethodName());
        //textEntryField.setText(getResources().getString(R.string.default_text));
    }

    /**
     * Sample handler that will be called from the secret "Edit Text" voice command (defined in VoiceCmdReceiver.java)
     */
    public void SelectTextBox() {
        Log.e(LOG_TAG, getMethodName());
        //textEntryField.requestFocus();
        // Show soft keyboard for the user to enter the value.
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.showSoftInput(textEntryField, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * A callback for the SDK to notify us if the recognizer starts or stop listening
     *
     * @param isRecognizerActive boolean - true when listening
     */
    public void RecognizerChangeCallback(boolean isRecognizerActive) {
        mRecognizerActive = isRecognizerActive;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateListenButtonText();
            }
        });
    }

    public void initSecondScanner() {
        Intent scannerIntent = new Intent(ScannerIntent.ACTION);
        try {
            // The Vuzix M300 has a built-in Barcode Scanner app that is registered for this intent.
            startActivityForResult(scannerIntent, PART_CODE_SCAN);
        } catch (ActivityNotFoundException activityNotFound) {
            Toast.makeText(this, R.string.only_on_m300, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * You may prefer using explicit intents for each recognized phrase. This receiver demonstrates that.
     */
    private MyIntentReceiver  myIntentReceiver;

    public class MyIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(LOG_TAG, getMethodName());
            Toast.makeText(context, "Custom Intent Detected", Toast.LENGTH_LONG).show();
        }
    }

    public void showDialog(String message) {
        alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        initSecondScanner();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void OnDone() {
        Log.d("Main", "Done");
        //alertDialog.dismiss();
    }
}
