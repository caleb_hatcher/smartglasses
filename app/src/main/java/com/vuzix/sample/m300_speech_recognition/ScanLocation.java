package com.vuzix.sample.m300_speech_recognition;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vuzix.sdk.barcode.ScanResult;
import com.vuzix.sdk.barcode.ScannerIntent;

public class ScanLocation extends Activity {
    private EditText scanField;
    private static final int REQUEST_CODE_SCAN = 90003;  // Must be unique within this Activity
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_location);

        scanField = (EditText) findViewById(R.id.scan_text_field);
        nextButton = (Button) findViewById(R.id.next_button);
    }

    /**
     * Handler for the button press. Activates the scan.
     */
    public void OnScanClick(View view) {
        Intent scannerIntent = new Intent(ScannerIntent.ACTION);
        try {
            // The Vuzix M300 has a built-in Barcode Scanner app that is registered for this intent.
            startActivityForResult(scannerIntent, REQUEST_CODE_SCAN);
        } catch (ActivityNotFoundException activityNotFound) {
            Toast.makeText(this, R.string.only_on_m300, Toast.LENGTH_LONG).show();
        }
    }

    public void OpenPartScan(View view) {
        Log.e("Scan Location", "OpenPartSCa");
        Intent startNewActivity = new Intent(this, SecondPartScan.class);
        startActivity(startNewActivity);
        //this.finish();
    }

    /**
     * The M300 Barcode Scanner App will scan a barcode and return
     *
     * @param requestCode int identifier you provided in startActivityForResult
     * @param resultCode int result of the scan operation
     * @param data Intent containing a ScanResult whenver the resultCode indicates RESULT_OK
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SCAN:
                if (resultCode == Activity.RESULT_OK) {
                    ScanResult scanResult = data.getParcelableExtra(ScannerIntent.RESULT_EXTRA_SCAN_RESULT);
                    Log.e("Scan", "Text:: " + scanResult.getText());
                    Variables.location = scanResult.getText();
                    scanField.setText( scanResult.getText() );
                    nextButton.setVisibility(View.VISIBLE);
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
