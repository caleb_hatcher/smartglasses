package com.vuzix.sample.m300_speech_recognition;

/**
 * Created by caleb on 7/25/18.
 */

public interface VoiceActionListener {
    void OnPopupClick();
    void OnRestoreClick();
    void OnClearClick();
    void SelectTextBox();
    void OnEnterClick();
    void NextPage();
    void OnDone();
    //void RecognizerChangeCallback(Boolean b);
}
